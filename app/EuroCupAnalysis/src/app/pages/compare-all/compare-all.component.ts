import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

declare var loadD3ScatterMatrix: any;
declare var loadD3ParaCoords: any;

@Component({
  selector: 'app-compare-all',
  templateUrl: './compare-all.component.html',
  styleUrls: ['./compare-all.component.css']
})
export class CompareAllComponent implements AfterViewInit {

  @ViewChild('scatterMatrixSection', { static: false }) scatterMatrixSection: ElementRef;
  @ViewChild('paraCoordsSection', { static: false }) paraCoordsSection: ElementRef;

  scatterMatrixJsonFilename = 'assets/scatter_matrix_';
  allJsonFilename = 'assets/scatter_matrix_all.json';
  paraCoordsJsonFilename = 'assets/para_coords.json';

  visualizationOptionSelected = "scattermatrix"

  gdpChecked = true
  gdpPerCapitaChecked = true
  populationChecked = true
  nbParticipationsChecked = true

  allChecked = false;
  selectedYear = 2016;

  constructor(private http: HttpClient) { }

  ngAfterViewInit() {
    this.updateCharts();
  }

  updateCharts() {
    if (this.visualizationOptionSelected == "scattermatrix") {
      this.paraCoordsSection.nativeElement.innerHTML = ''
      this.updateScatterMatrix()
    }
    else if (this.visualizationOptionSelected == "paracoords") {
      this.scatterMatrixSection.nativeElement.innerHTML = ''
      this.updateParaCoords()
    }
  }

  updateScatterMatrix() {
    this.scatterMatrixSection.nativeElement.innerHTML = '';
    let jsonFilename = this.scatterMatrixJsonFilename + this.selectedYear + '.json';
    if (this.allChecked) { jsonFilename = this.allJsonFilename; }
    this.http.get(jsonFilename).subscribe((json) => {
      if (this.gdpChecked || this.gdpPerCapitaChecked || this.populationChecked || this.nbParticipationsChecked) {
        loadD3ScatterMatrix(this.filterJson(json), innerWidth);
      }
    });
  }

  updateParaCoords() {
    this.paraCoordsSection.nativeElement.innerHTML = '';
    let jsonFilename = this.scatterMatrixJsonFilename + this.selectedYear + '.json';
    if (this.allChecked) { jsonFilename = this.allJsonFilename; }
    this.http.get(jsonFilename).subscribe((json) => {
      if (this.gdpChecked || this.gdpPerCapitaChecked || this.populationChecked || this.nbParticipationsChecked) {
        loadD3ParaCoords(this.filterJson(json), innerWidth);
      }
    });
  }

  filterJson(json): any {
    json.forEach((v) => {
      if (!this.gdpChecked) delete v['GDP ($)']
      if (!this.gdpPerCapitaChecked) delete v['GDP per capita ($)']
      if (!this.populationChecked) delete v['Population']
      if (!this.nbParticipationsChecked) delete v['Number of participations']
    });
    return json;
  }

  radioChange() {
    this.updateCharts()
  }
}
