import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartType, RadialChartOptions, ChartOptions } from 'chart.js';
import { Label, Color } from 'ng2-charts';
import * as GLOBALS from './../../globals';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {
  selectedYear = 2016;
  radarChartAllData = {};
  countryGeneralInfos = { nb_part: 0, 'titles won': 0, best_phase: 0 };


  selectedLeftYaxisValue = 'GDP';
  lineChartAllData = {
    GDP: [],
    'GDP per capita': [],
    Population: [],
    'Phase reached': []
  };

  country = '';

  jsonData = {};

  gdpReadable = '';
  gdpPerCapitaReadable = '';
  populationReadable = '';
  phaseReachedReadable = '';

  public radarChartOptions: RadialChartOptions = {
    responsive: true,
    scale: {
      ticks: {
        min: 0,
        max: 1,
        display: false
      },
    },
    tooltips: {
      callbacks: {
        // tslint:disable-next-line:no-string-literal
        label: tooltipItem => {
          let returnValue;
          const value = this.radarChartData[0]['data_not_normalized'][tooltipItem.index];
          switch (tooltipItem.index) {
            case 0: // GDP
              returnValue = (+value / Math.pow(10, 9)).toFixed(0);
              return returnValue + ' Billion USD';
            case 1: // GDP per capita
              returnValue = (value).toFixed(0);
              return returnValue + ' USD';
            case 2: // Population
              returnValue = (value / Math.pow(10, 6)).toFixed(1);
              return returnValue + ' Million';
            case 3: // Nb participations
              return value + '';
          }
        },
        title: tooltipItem => this.radarChartLabels[tooltipItem[0].index],
      }
    }
  };
  public radarChartLabels: Label[] = ['GDP', 'GDP per capita', 'Population', 'Number of participations'];

  public radarChartData: ChartDataSets[] = [];
  public radarChartType: ChartType = 'radar';
  public radarChartPlugins = [{
    beforeDraw(chart, easing) {
      const ctx = chart.ctx;
      const chartArea = chart.chartArea;

      ctx.save();
      ctx.fillStyle = 'white';

      ctx.fillRect(0, 0, chartArea.right + chartArea.left, chartArea.bottom + chartArea.top);
      ctx.restore();
    }
  }];

  public lineChartData: ChartDataSets[] = [];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    tooltips: {
      callbacks: {
        // tslint:disable-next-line:no-string-literal
        label: tooltipItem => {
          if (tooltipItem.datasetIndex === 0) {
            let value;
            switch (this.selectedLeftYaxisValue) {
              case 'GDP':
                value = (+tooltipItem.value / Math.pow(10, 9)).toFixed(0);
                return value + ' Billion USD';
              case 'GDP per capita':
                value = (+tooltipItem.value).toFixed(0);
                return value + ' USD';
              case 'Population':
                value = (+tooltipItem.value / Math.pow(10, 6)).toFixed(1);
                return value + ' Million';
            }
          } else {
            return this.convertPhaseReached(+tooltipItem.value);
          }
        }
      }
    },
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          ticks: {
            callback: (label, index, labels) => {
              let value;
              switch (this.selectedLeftYaxisValue) {
                case 'GDP':
                  value = label / Math.pow(10, 9);
                  return value + ' Billion USD';
                case 'GDP per capita':
                  value = label;
                  return value + ' USD';
                case 'Population':
                  value = label / Math.pow(10, 6);
                  return value + ' Million';
              }

            }
          }
        },
        {
          id: 'y-axis-1',
          position: 'right',
          gridLines: {
            color: 'rgba(255,0,0,0.3)',
          },
          ticks: {
            fontColor: 'red',
            min: 0,
            max: 5,
            stepSize: 1,
            callback: (label, index, labels) => this.convertPhaseReached(label)
          },
        }
      ]
    },
    annotation: {},
  };
  public lineChartColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // red
      backgroundColor: 'rgba(255,0,0,0.3)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [{
    beforeDraw(chart, easing) {
      const ctx = chart.ctx;
      const chartArea = chart.chartArea;
      const top = chartArea.top; // Use a value of 0 here to include the legend

      ctx.save();
      ctx.fillStyle = 'white';

      ctx.fillRect(0, 0, chartArea.right + chartArea.left, chartArea.bottom + chartArea.top);
      ctx.restore();
    }
  }];


  constructor(private route: ActivatedRoute, private router: Router) {
  }
  ngOnInit() {
    this.jsonData = GLOBALS.data;
    const arrayOfYears = Object.keys(this.jsonData);
    this.route.params.subscribe((params) => this.country = params.countryName);
    if (!GLOBALS.data[arrayOfYears[0]].hasOwnProperty(this.country)) {
      this.router.navigate(['404']);
    } else {
      this.loadCountryData();
    }
  }



  private loadCountryData() {
    const arrayOfYears = Object.keys(this.jsonData);

    this.radarChartAllData = {};
    this.lineChartLabels = arrayOfYears;
    this.countryGeneralInfos = GLOBALS.generalInfos[this.country];
    arrayOfYears.forEach((year) => {
      const countryData = this.jsonData[year][this.country];
      this.radarChartAllData[year] = [
        {
          data: [countryData.GDP_norm, countryData['GDP Capita_norm'], countryData.Population_norm,
          countryData.prev_part_norm], label: this.country, data_not_normalized: [countryData.GDP, countryData['GDP Capita'],
          countryData.Population, countryData.prev_part]
        }];

      this.lineChartAllData.GDP.push(countryData.GDP);
      this.lineChartAllData['GDP per capita'].push(countryData['GDP Capita']);
      this.lineChartAllData.Population.push(countryData.Population);
      this.lineChartAllData['Phase reached'].push(countryData.phase_reached);
    });

    this.radarChartData = this.radarChartAllData[this.selectedYear];
    this.gdpReadable = (this.jsonData[this.selectedYear][this.country].GDP / Math.pow(10, 9)).toFixed(0) + ' Billion USD';
    this.gdpPerCapitaReadable = this.jsonData[this.selectedYear][this.country]['GDP Capita'].toFixed(0) + ' USD';
    this.populationReadable = (this.jsonData[this.selectedYear][this.country].Population / Math.pow(10, 6)).toFixed(1) + ' Millions';
    this.phaseReachedReadable = this.convertPhaseReached(this.jsonData[this.selectedYear][this.country].phase_reached);

    this.lineChartData = [
      { data: this.lineChartAllData[this.selectedLeftYaxisValue], label: this.selectedLeftYaxisValue, fill: false },
      { data: this.lineChartAllData['Phase reached'], label: 'Phase reached', yAxisID: 'y-axis-1', fill: false }
    ];
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  selectedYearChanged(selectedYear) {
    this.gdpReadable = (this.jsonData[selectedYear][this.country].GDP / Math.pow(10, 9)).toFixed(0) + ' Billion USD';
    this.gdpPerCapitaReadable = this.jsonData[selectedYear][this.country]['GDP Capita'].toFixed(0) + ' USD';
    this.populationReadable = (this.jsonData[selectedYear][this.country].Population / Math.pow(10, 6)).toFixed(1) + ' Millions';
    this.phaseReachedReadable = this.convertPhaseReached(this.jsonData[selectedYear][this.country].phase_reached);
    this.radarChartData = this.radarChartAllData[selectedYear];
  }

  leftYaxisValueChanged(event) {
    this.lineChartData = [
      { data: this.lineChartAllData[this.selectedLeftYaxisValue], label: this.selectedLeftYaxisValue, fill: false },
      { data: this.lineChartAllData['Phase reached'], label: 'Phase reached', yAxisID: 'y-axis-1', fill: false }
    ];
  }

  convertPhaseReached(phaseNumber) {
    switch (phaseNumber) {
      case 0:
        return 'Non-qualified';
      case 1:
        return 'Qualified';
      case 2:
        return 'Quarter-finalist';
      case 3:
        return 'Semi-finalist';
      case 4:
        return 'Finalist';
      case 5:
        return 'Winner';
    }
  }

}
