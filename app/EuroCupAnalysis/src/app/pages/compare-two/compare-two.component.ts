import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import * as GLOBALS from './../../globals';
import { Label } from 'ng2-charts';
import { ChartDataSets, ChartType, RadialChartOptions } from 'chart.js';

@Component({
  selector: 'app-compare-two',
  templateUrl: './compare-two.component.html',
  styleUrls: ['./compare-two.component.css']
})
// tslint:disable:no-string-literal
export class CompareTwoComponent implements OnInit {

  firstCountryControl = new FormControl();
  secondCountryControl = new FormControl();
  countries = ['Switzerland', 'France', 'Italy'];
  filteredFirstCountries: Observable<string[]>;
  filteredSecondCountries: Observable<string[]>;

  firstSelectedCountry = '';
  secondSelectedCountry = '';

  firstCountryReadableValues = {
    GDP: '',
    GDP_per_capita: '',
    population: '',
    phase_reached: ''
  };
  secondCountryReadableValues = {
    GDP: '',
    GDP_per_capita: '',
    population: '',
    phase_reached: ''
  };

  firstCountryData = {};
  secondCountryData = {};

  selectedYear = 2016;
  radarChartAllData = {};
  dataLoaded = false;

  jsonData = GLOBALS.data;

  public radarChartOptions: RadialChartOptions = {
    responsive: true,
    scale: {
      ticks: {
        min: 0,
        max: 1,
        display: false
      },
    },
    tooltips: {
      callbacks: {
        // tslint:disable-next-line:no-string-literal
        label: tooltipItem => {
          let returnValue;
          const value = this.radarChartData[tooltipItem.datasetIndex]['data_not_normalized'][tooltipItem.index];
          switch (tooltipItem.index) {
            case 0: // GDP
              returnValue = (+value / Math.pow(10, 9)).toFixed(0);
              return returnValue + ' Billion USD';
            case 1: // GDP per capita
              returnValue = (value).toFixed(0);
              return returnValue + ' USD';
            case 2: // Population
              returnValue = (value / Math.pow(10, 6)).toFixed(1);
              return returnValue + ' Million';
            case 3: // Nb participations
              return value + '';
          }
        },
        title: tooltipItem => this.radarChartLabels[tooltipItem[0].index],
      }
    }
  };
  public radarChartLabels: Label[] = ['GDP', 'GDP per capita', 'Population', 'Number of participations'];
  public radarChartData: ChartDataSets[] = [];
  public radarChartType: ChartType = 'radar';
  public radarChartPlugins = [{
    beforeDraw(chart, easing) {
      const ctx = chart.ctx;
      const chartArea = chart.chartArea;

      ctx.save();
      ctx.fillStyle = 'white';

      ctx.fillRect(0, 0, chartArea.right + chartArea.left, chartArea.bottom + chartArea.top);
      ctx.restore();
    }
  }];

  constructor() { }

  ngOnInit() {
    this.countries = Object.keys(GLOBALS.data[2016]);
    this.countries.sort((a, b) => a.localeCompare(b));

    this.filteredFirstCountries = this.firstCountryControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    this.filteredSecondCountries = this.secondCountryControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private loadCountriesData() {
    const arrayOfYears = Object.keys(this.jsonData);

    this.radarChartAllData = {};

    if (this.firstSelectedCountry === '' || this.secondSelectedCountry === '') {
      return;
    }

    arrayOfYears.forEach((year) => {
      this.firstCountryData = this.jsonData[year][this.firstSelectedCountry];
      this.secondCountryData = this.jsonData[year][this.secondSelectedCountry];

      this.radarChartAllData[year] = [
        {
          data: [this.firstCountryData['GDP_norm'], this.firstCountryData['GDP Capita_norm'], this.firstCountryData['Population_norm'],
          this.firstCountryData['prev_part_norm']], label: this.firstSelectedCountry, data_not_normalized: [this.firstCountryData['GDP'],
          this.firstCountryData['GDP Capita'], this.firstCountryData['Population'], this.firstCountryData['prev_part']]
        },
        {
          data: [this.secondCountryData['GDP_norm'], this.secondCountryData['GDP Capita_norm'], this.secondCountryData['Population_norm'],
          this.secondCountryData['prev_part_norm']], label: this.secondSelectedCountry,
          data_not_normalized: [this.secondCountryData['GDP'],
          this.secondCountryData['GDP Capita'], this.secondCountryData['Population'], this.secondCountryData['prev_part']]
        }];
    });

    this.radarChartData = this.radarChartAllData[this.selectedYear];

    this.firstCountryReadableValues.GDP =
      (this.jsonData[this.selectedYear][this.firstSelectedCountry].GDP / Math.pow(10, 9)).toFixed(0) + ' Billion USD';
    this.firstCountryReadableValues.GDP_per_capita =
      (this.jsonData[this.selectedYear][this.firstSelectedCountry]['GDP Capita']).toFixed(0) + ' USD';
    this.firstCountryReadableValues.population =
      (this.jsonData[this.selectedYear][this.firstSelectedCountry].Population / Math.pow(10, 6)).toFixed(1) + ' Millions';
    this.firstCountryReadableValues.phase_reached =
      this.convertPhaseReached(this.jsonData[this.selectedYear][this.firstSelectedCountry]['phase_reached']);

    this.secondCountryReadableValues.GDP =
      (this.jsonData[this.selectedYear][this.secondSelectedCountry].GDP / Math.pow(10, 9)).toFixed(0) + ' Billion USD';
    this.secondCountryReadableValues.GDP_per_capita =
      (this.jsonData[this.selectedYear][this.secondSelectedCountry]['GDP Capita']).toFixed(0) + ' USD';
    this.secondCountryReadableValues.population =
      (this.jsonData[this.selectedYear][this.secondSelectedCountry].Population / Math.pow(10, 6)).toFixed(1) + ' Millions';
    this.secondCountryReadableValues.phase_reached =
      this.convertPhaseReached(this.jsonData[this.selectedYear][this.secondSelectedCountry]['phase_reached']);

    this.dataLoaded = true;
    console.log(this.radarChartData);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return Object.assign([], this.countries).filter(country => country.toLowerCase().includes(filterValue));
  }

  onFirstCountrySelectChange(country) {
    this.firstSelectedCountry = country;
    this.loadCountriesData();
  }

  onSecondCountrySelectChange(country) {
    this.secondSelectedCountry = country;
    this.loadCountriesData();
  }

  selectedYearChanged(selectedYear) {
    this.firstCountryReadableValues.GDP =
      (this.jsonData[this.selectedYear][this.firstSelectedCountry].GDP / Math.pow(10, 9)).toFixed(0) + ' Billion USD';
    this.firstCountryReadableValues.GDP_per_capita =
      (this.jsonData[this.selectedYear][this.firstSelectedCountry]['GDP Capita']).toFixed(0) + ' USD';
    this.firstCountryReadableValues.population =
      (this.jsonData[this.selectedYear][this.firstSelectedCountry].Population / Math.pow(10, 6)).toFixed(1) + ' Millions';
    this.firstCountryReadableValues.phase_reached =
      this.convertPhaseReached(this.jsonData[this.selectedYear][this.firstSelectedCountry]['phase_reached']);

    this.secondCountryReadableValues.GDP =
      (this.jsonData[this.selectedYear][this.secondSelectedCountry].GDP / Math.pow(10, 9)).toFixed(0) + ' Billion USD';
    this.secondCountryReadableValues.GDP_per_capita =
      (this.jsonData[this.selectedYear][this.secondSelectedCountry]['GDP Capita']).toFixed(0) + ' USD';
    this.secondCountryReadableValues.population =
      (this.jsonData[this.selectedYear][this.secondSelectedCountry].Population / Math.pow(10, 6)).toFixed(1) + ' Millions';
    this.secondCountryReadableValues.phase_reached =
      this.convertPhaseReached(this.jsonData[this.selectedYear][this.secondSelectedCountry]['phase_reached']);

    this.radarChartData = this.radarChartAllData[selectedYear];
  }

  convertPhaseReached(phaseNumber) {
    switch (phaseNumber) {
      case 0:
        return 'Non-qualified';
      case 1:
        return 'Qualified';
      case 2:
        return 'Quarter-finalist';
      case 3:
        return 'Semi-finalist';
      case 4:
        return 'Finalist';
      case 5:
        return 'Winner';
    }
  }
}
