import { Component, OnInit, HostListener, ElementRef, ViewChild, NgZone, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import * as GLOBALS from './../../globals';

declare var jQuery: any

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements AfterViewInit {

  countries = Object.keys(GLOBALS.data[2016]);

  cursorOnCountry = false
  country: string = 'None'
  nbParticipations = '';
  nbTitles = '';
  bestPhaseReached = '';

  constructor(private router: Router, private ngZone: NgZone) {
    this.countries.sort((a, b) => a.localeCompare(b));
  }

  ngAfterViewInit() {
    jQuery('#europe-map').vectorMap({
      map: 'europe_mill',
      onRegionTipShow: (e, el, code) => this.onRegionTipShow(e, el, code),
      onRegionClick: (e, code) => this.onRegionClick(e, code),
      onRegionOut: (e, code) => this.onRegionOut(e, code)
    })
  }

  onRegionTipShow(e, el, code) {
    this.cursorOnCountry = true
    var map = jQuery('#europe-map').vectorMap('get', 'mapObject')
    var regionName = map.getRegionName(code)
    if (regionName == 'United Kingdom') regionName = "England"
    this.country = regionName
    if (GLOBALS.generalInfos[this.country] !== undefined) {
      this.nbParticipations = GLOBALS.generalInfos[this.country].nb_part;
      this.nbTitles = GLOBALS.generalInfos[this.country]['titles won'];
      this.bestPhaseReached = GLOBALS.generalInfos[this.country].best_phase;
    } else {
      this.nbParticipations = '-'
      this.nbTitles = '-'
      this.bestPhaseReached = '-'
    }
  }

  onRegionClick(e, code) {
    var map = jQuery('#europe-map').vectorMap('get', 'mapObject')
    var regionName = map.getRegionName(code)
    if (regionName == 'United Kingdom') regionName = "England"
    this.router.navigate([regionName])
  }

  onRegionOut(e, code) {
    var map = jQuery('#europe-map').vectorMap('get', 'mapObject')
    this.cursorOnCountry = false
  }

  mouseOverItem(country) {
    this.cursorOnCountry = true
    this.country = country
    if (GLOBALS.generalInfos[this.country] !== undefined) {
      this.nbParticipations = GLOBALS.generalInfos[this.country].nb_part;
      this.nbTitles = GLOBALS.generalInfos[this.country]['titles won'];
      this.bestPhaseReached = GLOBALS.generalInfos[this.country].best_phase;
    } else {
      this.nbParticipations = '-'
      this.nbTitles = '-'
      this.bestPhaseReached = '-'
    }

  }

  mouseLeave() {
    this.cursorOnCountry = false
  }
}
