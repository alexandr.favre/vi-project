import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { CountryComponent } from './pages/country/country.component';
import { CompareTwoComponent } from './pages/compare-two/compare-two.component';
import { CompareAllComponent } from './pages/compare-all/compare-all.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'compare_two', component: CompareTwoComponent },
  { path: 'compare_all', component: CompareAllComponent },
  { path: '404', component: NotFoundComponent },
  { path: ':countryName', component: CountryComponent },
  { path: '**', redirectTo: '/404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
