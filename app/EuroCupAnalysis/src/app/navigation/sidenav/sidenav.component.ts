import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  @Output() private sidenavClose = new EventEmitter()

  constructor(private router: Router) { }

  ngOnInit() { }

  public onSidenavClose = () => {
    this.sidenavClose.emit()
  }
}
