
function loadD3ParaCoords(data, windowWidth) {

    var labels = ["winner", "finalist", "semi-finalist", "quarter-finalist", "qualified", "non-qualified"]

    // set the dimensions and margins of the graph
    var margin = { top: 30, right: 70, bottom: 10, left: 70 },
        width = windowWidth / 3 * 2 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;

    // append the svg object to the body of the page
    var svg = d3.select("#paraCoordsSection")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    // Color scale: give me a specie name, I return a color
    var color = d3.scaleOrdinal()
        .domain(["5", "4", "3", "2", "1", "0"])
        .range(["#d62728", "#1f77b4", "#9467bd", "#2ca02c", "#ff7f0e", "#8c564b"])

    // Here I set the list of dimension manually to control the order of axis:
    dimensions = d3.keys(data[0]).filter(function (d) { return d !== "Phase reached"; })
    dimensions.push("Phase reached")

    // For each dimension, I build a linear scale. I store all in a y object
    var y = {}
    var scales = { "Population": 200000000, "GDP ($)": 4000000000000, "GDP per capita ($)": 120000, "Number of participations": 15, "Phase reached": 5 }
    for (i in dimensions) {
        name = dimensions[i]
        y[name] = d3.scaleLinear()
            .domain([0, scales[name]]) // --> Same axis range for each group
            // --> different axis range for each group --> .domain( [d3.extent(data, function(d) { return +d[name]; })] )
            .range([height, 0])
    }

    // Build the X scale -> it find the best position for each Y axis
    x = d3.scalePoint()
        .range([0, width])
        .domain(dimensions);

    // Highlight the specie that is hovered
    var highlight = function (d) {

        selected_specie = d['Phase reached']

        // first every group turns grey
        d3.selectAll(".line")
            .transition().duration(200)
            .style("stroke", "lightgrey")
            .style("opacity", "0.2")
        // Second the hovered specie takes its color
        d3.selectAll("." + labels[selected_specie])
            .transition().duration(200)
            .style("stroke", color(selected_specie))
            .style("opacity", "1")
    }

    // Unhighlight
    var doNotHighlight = function (d) {
        d3.selectAll(".line")
            .transition().duration(200).delay(1000)
            .style("stroke", function (d) { return (color(d['Phase reached'])) })
            .style("opacity", "1")
    }

    // The path function take a row of the csv as input, and return x and y coordinates of the line to draw for this raw.
    function path(d) {
        return d3.line()(dimensions.map(function (p) { return [x(p), y[p](d[p])]; }));
    }

    // Draw the lines
    svg
        .selectAll("myPath")
        .data(data)
        .enter()
        .append("path")
        .attr("class", function (d) { return "line " + labels[d['Phase reached']] }) // 2 class for each line: 'line' and the group name
        .attr("d", path)
        .style("fill", "none")
        .style("stroke", function (d) { return (color(d['Phase reached'])) })
        .style("opacity", 0.5)
        .on("mouseover", highlight)
        .on("mouseleave", doNotHighlight)

    var formatAxis = d3.format('.2s');
    function tickFormat(num) {
        return formatAxis(num)
            .replace("5.0", labels[0])
            .replace("4.0", labels[1])
            .replace("3.0", labels[2])
            .replace("2.0", labels[3])
            .replace("1.0", labels[4])
            .replace("0.0", labels[5]);
    }

    // Draw the axis:
    svg.selectAll("myAxis")
        // For each dimension of the dataset I add a 'g' element:
        .data(dimensions).enter()
        .append("g")
        .attr("class", "axis2")
        // I translate this element to its right position on the x axis
        .attr("transform", function (d) { return "translate(" + x(d) + ")"; })
        // And I build the axis with the call function
        .each(function (d) {
            if (d == "Phase reached") d3.select(this).call(d3.axisLeft().tickFormat(tickFormat).ticks(5).scale(y[d]));
            else d3.select(this).call(d3.axisLeft().tickFormat(formatAxis).ticks(5).scale(y[d]));
        })
        // Add axis title
        .append("text")
        .style("text-anchor", "middle")
        .attr("y", -9)
        .text(function (d) { return d; })
        .style("fill", "black")
}