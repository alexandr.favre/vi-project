# Euro Cup Analysis
**Charles Perriard, Alexandre Favre & Marco Mattei**

## Installation

Tout d'abord, le projet a été mis en ligne à l'adresse suivante : http://35.180.207.173:4200

Si par malheur il venait à ne pas fonctionner, il faut se rendre dans le dossier "app/EuroCupAnalysis" et exécuter les commandes suivantes :
```
npm install
ng serve --open
```


## Intention et question de base.
En regardant la liste des derniers vainqueurs des compétitions internationales
de football, nous avons remarqué qu'il était extrêmement rare d'y trouver des
pays avec une faible population ou des pays pauvres. Fort de ce constat, nous
nous sommes demandés **quels pouvaient être les facteurs qui
influençaient sur la réussite d'une équipe au championnat d'Europe
de football** et plus particulièrement si la richesse d'un pays (représentée par
son PIB et son PIB/habitant) et sa population impactaient ses résultats. Par la
suite, nous avons également pris en compte une variable supplémentaire qui
nous paraissait pertinente, l'expérience passée d'une nation à la dite
compétition.

L'objectif de notre projet est donc d'informer, principalement les fans de
football, sur les paramètres extra-sportifs d'une équipe et la manière avec
laquelle ils influencent ses performances.

## Hypothèses
Avant de nous plonger dans le développement de notre application et dans
l'analyse des différents graphes réalisés, nous avons émis différentes
hypothèses. D'après nous:

* Les pays capables de briller à l'Euro sont les pays possédant un PIB/habitant
  élevé puisque celui-ci permet à l'Etat de mettre à disposition de son équipe
  des équipements de qualité et d'investir efficacement dans la formation.
* La population d'un pays joue un rôle léger dans les performances d'une
  équipe puisqu'elle influence directement le nombre de footballeurs à
  disposition.
* Le PIB d'un pays n'influence pas forcément les résultats d'une équipe
  puisqu'il est plus facile pour un pays avec une population importante
  d'atteindre un PIB important, et vice versa.
* Plus un pays participe régulièrement à un Euro, plus il brille dans celui-ci
  puisqu'il apprend de ses erreurs et appréhende la compétition avec plus
  d'expérience. Nous supposons cependant que l'impact reste léger.

## Choix des données
Dans un premier temps, nous avions souhaité réaliser la même application mais
en utilisant les données concernant les Coupes du Monde de football. Cependant,
les datasets proposant les PIB et les populations des différents pays étaient
trop incomplets. Cela n'a pas vraiment été un problème puisque l'Europe contient
de nombreux pays et surtout, les datasets étaient complets.

Les données concernant la population et le PIB ont été récupérés sur
https://data.worldbank.org/

Les données concernant les phases atteintes et l'expérience des pays à l'Euro
ont été construites manuellement à l'aide des pages Wikipedia de chaque Euro.

## Choix des graphes, interactions et explications

Nous avons mis en place différents graphes afin de confirmer ou d'infirmer nos
hypothèses.

### Carte de l'Europe
Pour pouvoir sélectionner un pays à analyser, nous avons décidé d'utiliser une carte de l'Europe. En effet, ceci permet de rapidement cliquer sur un pays si on sait où il se trouve sur la carte plutôt que de le rechercher dans une longue liste. Cependant, pour certains pays, il est parfois difficile de savoir où il se trouve. C'est pourquoi nous avons également affiché à côté de la carte une liste des pays dans l'ordre alphabétique.

Les intéractions sont les suivantes : 
* Passer la souris par dessus un pays ou un nom de pays (hover) : Le pays est mis en évidence et des informations générales sont affichées par rapport à celui-ci dans une sous-fenêtre (Nb de participations, nb de titres, meilleure phase atteinte)
* Cliquer sur un pays ou un nom de pays : L'application ouvre la page des informations et graphes du pays sélectionné.

'Hover' sur un pays de la carte          |  'Hover' sur un pays la liste
:-------------------------:|:-------------------------:
![](https://i.imgur.com/kD6KETD.png)  |  ![](https://i.imgur.com/JLPxGV6.png)
### Line chart (graphique linéaire) avec deux axes y
Pour chaque pays, il est possible de comparer l'évolution du PIB,
du PIB/habitant et de la population avec l'évolution de ses performances.
L'objectif était de voir si les courbes suivaient la même courbe ou non.
Un menu déroulant permet à l'utilisateur de modifier la variable de comparaison.

<img src="https://i.imgur.com/mAO0poP.jpg"  width="55%" height="55%">

### Radar
Pour chaque pays, il est possible de visualiser ses tendances sur un graphe en
radar. Le tout couplé avec un slider, l'utilisateur peut ainsi voir l'évolution
de l'ensemble des caractéristiques du pays au fil des années et ainsi voir si
l'évolution commune de plusieurs caractéristiques influence sur ses performances.

<img src="https://i.imgur.com/QWmkYgi.png"  width="55%" height="55%">

### Radar chart comparatif
Le graphe en radar a également été utilisé pour comparer deux pays.
Ainsi, il est possible de voir si un pays qui réussit mieux qu'un autre a systématiquement
une caractéristique clairement différente de l'autre au fil des années.
A nouveau, l'évolution au fil des Euros se fait à l'aide d'un slider horizontal.

<img src="https://i.imgur.com/soRlWgX.jpg"  width="55%" height="55%">

### Scatter Plot Matrix (SPLOM)
Le SPLOM a été utilisé pour comparer tous les pays. Il permet d'afficher un
graphe avec sur les axes les différentes variables (PIB, PIB/habitant, 
population, nombre de participations). La couleur des points va, elle, indiquer
la phase atteinte par le pays. Il est possible de filtrer les facteurs à afficher
ainsi que l'année voulue. L'objectif est ensuite de voir si, sur certains des scatterplot,
des points de même couleur se retrouvent regroupés dans une même zone. Si tel est le cas,
cela signifierait que les pays partageant les mêmes deux caractéristiques performent
de la même manière. De plus, il est possible de sélectionner un groupe de points dans l'un
des scatterplot afin de les mettre en évidence 

<img src="https://i.imgur.com/b2wQaZ8.jpg"  width="55%" height="55%">

### Coordonnées parallèles
Tout comme le SPLOM, le graphe des coordonnées parallèles a été utilisé pour comparer tous les pays simultanément.
Contrairement au SPLOM, il permet de visualiser l'ensemble des variables.
Ici, l'idée est de voir si les lignes d'une même couleur, c'est-à-dire les pays ayant atteint la même phase, se rejoignent sur les différents axes.
A nouveau, il est possible de filter les différentes variables et de modifier l'année. On peut égalemer survoler les lignes d'un même type pour les mettre en évidence.

<img src="https://i.imgur.com/WwYKOaX.jpg"  width="55%" height="55%">

## Outils
Au niveau des outils, nous avons choisi d'utiliser les suivants :
* [ng2-charts](https://valor-software.com/ng2-charts/#/GeneralInfo) (directives Angular pour [Chart.js](https://www.chartjs.org/)) pour les graphes "line chart" et "radar"
* [D3.js](https://d3js.org/) pour les graphes SPLOM et "coordonées parallèles"
* [jVectorMap](http://jvectormap.com/) pour la carte de l'Europe

Nous avons trouvé ces différents outils très performants et simples d'utilisation. Cependant il a été nécessaire de faire un certain nombre de paramétrages et de modifications afin d'arriver aux résultats attendus. Pour les "line chart" de Chart.js par exemple, les labels sur les différents axes ont dû être adaptés afin de rendre la lisibilité plus simple que de juste afficher les données bruttes. En effet, il est plus simple de lire "3000 Billion USD" que "3000000000000 USD". De même que les informations affichées sur les tooltips lors du passage de la souris sur un point des graphes du radar qui n'étaient pas forcément parlant de base et qui affichent les données bruttes. 
Par contre l'avantage à souligner de ces différents outils est qu'on peut paramétrer un grand nombre d'éléments afin d'arriver à nos souhaits.

## Résultats et réponses aux questions
La question principale sur laquelle portait se projet était la suivante:

**Quels sont les facteurs qui influent sur la réussite d'une équipe au championnat d'Europe de football**

Pour répondre à  cette question, nous avons analysé les différents graphes que nous avons implémenté.

<img src="https://i.imgur.com/WyLh9Bv.jpg"  width="55%" height="55%">

Sur le graphe ci-dessus, on constate que la courbe du PIB de l'allemagne colle parfaitement avec ses résultats.
Cependant, il s'agit là d'un cas unique. Nous avons répété l'expérience avec d'autres pays et on ne retrouve presque jamais
ce pattern, peu importe la variable choisie.


Grèce en 2000         |  Grèce en 2004
:-------------------------:|:-------------------------:
![](https://i.imgur.com/RafQIme.jpg)  |  ![](https://i.imgur.com/QlhvmWo.jpg)

En analysant le radar au fil des Euros, on constate assez facilement que les résultats
varient énormément alors que les caractéristiques, elles, ne changent quasiment pas.
L'exemple ci-dessous montre qu'avec plus ou moins les mêmes valeurs, la Grèce ne
s'était pas qualifiée en 2000 alors qu'elle a remporté l'Euro en 2004.
Ce constat a pu être fait pour de nombreux pays.

<img src="https://i.imgur.com/LZk0mxe.jpg"  width="55%" height="55%">

Le radar comparatif, lui, permet de faire le même constat que précédemment.
Certains pays affichant des PIB ou encore des PIB/habitant beaucoup plus élevés
qu'un autre n'obtiennent pas forcément des meilleurs résultats à la compétition

Concernant la comparaison de tous les pays à l'aide du SPLOM, on peut voir sur le scatter plot qui prend la population et le PIB/habitant que les pays sont relativements bien "clusterisés" concernant le fait qu'ils ne se soient pas qualifiés (brun) ou qualifiés (toutes les autres couleurs). En effet, on aperçoit facilement que les pays ayant une population et un PIB/habitant faibles se qualifient rarement contrairement aux autres. Ensuite, par rapport à la phase atteinte précisément lors de la qualification, les données sont plus partagées et on peut moins dire que ces facteurs ont un impact sur la phase que le pays atteindra lors de la compétition.

<img src="https://i.imgur.com/aDGBn7Y.png"  width="55%" height="55%">

Sur le graphe de coordonnées parallèles, on voit également que les pays bruns (les pays non qualifiés) combinent
dans la plupart des cas un PIB/habitant faible et une population faible.

<img src="https://i.imgur.com/74mLwkP.jpgg"  width="75%" height="75%">

De plus, on constate également qu'aucun pays vainqueur n'affichait un PIB/habitant plus petit que 20'000$.

<img src="https://i.imgur.com/7eTNrk0.jpg"  width="75%" height="75%">

Les différentes analyses menées sur les graphes indiquent clairement que les pays
réussisant bien à l'Euro possèdent, dans la plupart des cas, une population élevée,
un PIB/habitant élevé et un PIB élevé. Il doit cependant être noté que le PIB d'un
pays et sa population sont des variables dépendantes. De même, il est difficile
de conclure qu'un nombre de participations élevé améliore les performances puisque,
forcément, les bonnes équipes participeront plus souvent.

Nos hypothèses se sont révélées être en partie vraies. En effet, les pays à faible richesse ou faible population ne se qualifient que rarement à ces compétitions.
Cependant, une fois que l'on se concentre sur les pays s'étant qualifiés, on constate que la richesse et la population ne suffisent plus. Le succès au football, comme dans tous les sports
d'ailleurs, dépend d'une multitude de paramètres et croire que l'argent suffit pour s'y démarquer serait une grave erreur. La culture du football dans le pays, les stratégies
utilisées ou tout simplement la chance semblent être les paramètres les plus à même d'influer sur l'issue finale d'un match. Et ça, c'est une bonne nouvelle pour le sport.
